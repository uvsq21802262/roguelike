package uvsq.m1info.roguelike.util;

import java.util.List;

import uvsq.m1info.roguelike.agents.BatAi;
import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.agents.FungusAi;
import uvsq.m1info.roguelike.agents.PlayerAi;
import uvsq.m1info.roguelike.agents.ZombieAi;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.map.World;
import asciiPanel.AsciiPanel;

public class StuffFactory {
	private World world;

    public StuffFactory(World world){
        this.world = world;
    }
    
    public Creature newPlayer(List<String> messages, int depth){
	    Creature player = new Creature(world, "player", '@', AsciiPanel.brightWhite, 10, 20, 5);
	    world.addAtEmptyLocation(player, depth);
	    new PlayerAi(player, messages);
	    return player;
    }
    
    public Creature newFungus(int depth){
        Creature fungus = new Creature(world, "fungus", 'f', AsciiPanel.green, 10, 0, 0);
        world.addAtEmptyLocation(fungus, depth);
        new FungusAi(fungus, this);
        return fungus;
    }
    
    public Creature newBat(int depth){
        Creature bat = new Creature(world, "bat", 'b', AsciiPanel.red, 15, 5, 0);
        world.addAtEmptyLocation(bat, depth);
        new BatAi(bat);
        return bat;
    }
    
    public Item newRock(int depth){
        Item rock = new Item(',', AsciiPanel.yellow, "rock");
        rock.modifyFoodValue(-1);
        world.addAtEmptyLocation(rock, depth);
        return rock;
    }
    
    public Item newVictoryItem(int depth){
        Item item = new Item('*', AsciiPanel.brightWhite, "teddy bear");
        world.addAtEmptyLocation(item, depth);
        return item;
    }
    
    public Item newDagger(int depth){
        Item item = new Item(')', AsciiPanel.white, "dagger");
        item.modifyAttackValue(5);
        world.addAtEmptyLocation(item, depth);
        return item;
      }
    public Item newSword(int depth){
        Item item = new Item(')', AsciiPanel.brightWhite, "sword");
        item.modifyAttackValue(10);
        world.addAtEmptyLocation(item, depth);
        return item;
      }

    public Item newStaff(int depth){
	    Item item = new Item(')', AsciiPanel.yellow, "staff");
	    item.modifyAttackValue(5);
	    item.modifyDefenseValue(3);
	    world.addAtEmptyLocation(item, depth);
	    return item;
    }

    public Item newLightArmor(int depth){
	    Item item = new Item('[', AsciiPanel.green, "tunic");
	    item.modifyDefenseValue(2);
	    world.addAtEmptyLocation(item, depth);
	    return item;
    }

	public Item newMediumArmor(int depth){
	    Item item = new Item('[', AsciiPanel.white, "chainmail");
	    item.modifyDefenseValue(4);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	}

	public Item newHeavyArmor(int depth){
	    Item item = new Item('[', AsciiPanel.brightWhite, "platemail");
	    item.modifyDefenseValue(6);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	}

	public Item randomWeapon(int depth){
	    switch ((int)(Math.random() * 4)){
		    case 0: return newDagger(depth);
		    case 1: return newSword(depth);
		    default: return newStaff(depth);
	    }
	}

	public Item randomArmor(int depth){
    	switch ((int)(Math.random() * 3)){
        	case 0: return newLightArmor(depth);
        	case 1: return newMediumArmor(depth);
        	default: return newHeavyArmor(depth);
    	}
	}
      
	public Item newEdibleWeapon(int depth){
		Item item = new Item(')', AsciiPanel.yellow, "baguette");
		item.modifyAttackValue(3);
		item.modifyFoodValue(50);
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Creature newZombie(int depth, Creature player){
	      Creature zombie = new Creature(world, "zombie", 'z', AsciiPanel.white, 50, 10, 10);
	      world.addAtEmptyLocation(zombie, depth);
	      new ZombieAi(zombie, player);
	      zombie.setVision(5);
	      return zombie;
	  }
}
