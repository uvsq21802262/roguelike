package uvsq.m1info.roguelike.agents;

import uvsq.m1info.roguelike.util.StringOperations;

import java.awt.Color;

import uvsq.m1info.roguelike.items.Inventory;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.level.LevelUpController;
import uvsq.m1info.roguelike.map.Location;
import uvsq.m1info.roguelike.map.Point;
import uvsq.m1info.roguelike.map.Tile;
import uvsq.m1info.roguelike.map.World;

public class Creature implements Location{
	private final int INVENTORY_SIZE = 20;
	private final int MAX_FOOD = 1000;
    private World world;
    public Point point;
    private CreatureAi ai;
    private String name;
    private char glyph;
    private Color color;
    private int maxHp;
    private int hp;
    private int attackValue;
    private int defenseValue;
    private Inventory inventory;
    private int maxFood;
    private int food;
    private Item weapon;
    private Item armor;
    private int visionRadius;
    private int level;
    private int xp;
    

    public Creature(World world, String name, char glyph, Color color, int maxHp, int attack, int defense) {
        this.world = world;
        this.name = name;
        this.glyph = glyph;
        this.color = color;
        this.maxHp = maxHp;
        this.hp = maxHp;
        this.attackValue = attack;
        this.defenseValue = defense;
        this.inventory = new Inventory(INVENTORY_SIZE);
		this.maxFood = MAX_FOOD;
		this.food = maxFood * 2/3;
		this.visionRadius = 0;
		this.level = 1;
    }
    
    public void setPoint(Point p) { point = p; }
    
    public void setX(int x) { point.x = x; }
    
    public void setY(int y) { point.y = y; }
    
    public void setZ(int z) { point.z = z; }
    
    public int getX() {	return point.x; }
    
    public int getY() { return point.y; }
    
    public int getZ() { return point.z;  }
	
    public Location location() { return point; }
	
    public void setCreatureAi(CreatureAi ai) { this.ai = ai; }

    public char glyph() { return glyph; }

    public Color color() { return color; }
    
    public Creature creature(int wx, int wy, int wz) { return world.creature(wx, wy, wz); }
    
    public void update() { 
        modifyFood(-1);
        ai.onUpdate();
    }
    public boolean canEnter(int wx, int wy, int wz) {
    	return world.tile(wx, wy, wz).isGround() && world.creature(wx, wy, wz) == null;
    }
    
    public void moveBy(int mx, int my, int mz){
    	if (mx==0 && my==0 && mz==0)
    	    return;
    	Tile tile = world.tile(point.x + mx, point.y + my, point.z + mz);
    	if (mz == -1){
            if (tile == Tile.STAIRS_DOWN) {
                doAction("walk up the stairs to level %d", point.z + mz + 1);
            } else {
                doAction("try to go up but are stopped by the cave ceiling");
                return;
            }
        } else if (mz == 1){
            if (tile == Tile.STAIRS_UP) {
                doAction("walk down the stairs to level %d", point.z + mz + 1);
            } else {
                doAction("try to go down but are stopped by the cave floor");
                return;
            }
        }
        Creature other = world.creature(point.x + mx, point.y + my, point.z + mz);
      
        if (other == null)
            ai.onEnter(point.x + mx, point.y + my, point.z + mz, world.tile(point.x + mx, point.y + my, point.z + mz));
        else
            attack(other);
    }
    
    public void dig(int wx, int wy) {
    	modifyFood(-10);
        world.dig(wx, wy, point.z);
        doAction("dig");
    }
    
    public int maxHp() { return maxHp; }
    
    public int hp() { return hp; }
    
    public int attackValue() {
		return attackValue
		 + (weapon == null ? 0 : weapon.attackValue())
		 + (armor == null ? 0 : armor.attackValue());
      }

    public int defenseValue() {
    	return defenseValue
		 + (weapon == null ? 0 : weapon.defenseValue())
		 + (armor == null ? 0 : armor.defenseValue());
      }


    public void attack(Creature other){
        int amount = Math.max(0, attackValue() - other.defenseValue());
        amount = (int)(Math.random() * amount) + 1;
        other.modifyHp(-amount);
        doAction("attack the '%s' for %d damage", other.glyph, amount);

        if (other.hp < 1)
        	gainXp(other);

    }

    public void modifyHp(int amount) {
        hp += amount;
        if (hp < 1) {
            doAction("die");
            leaveCorpse();
            world.remove(this);
        }
    }

    public void notify(String message, Object ... params){
        ai.onNotify(String.format(message, params));
    }
    
    private String makeSecondPerson(String text){
        return StringOperations.toThirdPerson(text);
    }
    
    public void doAction(String message, Object ... params){
	    int r = 9;
	    ///*
	    for (int ox = -r; ox < r+1; ox++){
		    for (int oy = -r; oy < r+1; oy++){
		         if (ox*ox + oy*oy > r*r)
		             continue;
		         Creature other = world.creature(point.x + ox, point.y + oy, point.z);//meme niveau
		     
		         if (other == null)
		             continue;
		     
		         if (other == this)
		             other.notify("You " + message + ".", params);
		         else
		             other.notify(String.format("The '%s' %s.", glyph, makeSecondPerson(message)), params);
		    }
	    }
	    //*/
	    /*
 	    for (int i = 0; i < 81; i++){
 		    for (int j = 0; j < 20; j++){
 		         Creature other = world.creature(i, j, z);//meme niveau
 		     
 		         if (other == null)
 		             continue;
 		     
 		         if (other == this)
 		             other.notify("You " + message + ".", params);
 		         else
 		             other.notify(String.format("The '%s' %s.", glyph, makeSecondPerson(message)), params);
 		    }
 	    }
 	    */
    }
	
    public Inventory inventory() { return inventory; }
    
    public void pickup() {
        Item item = world.item(point.x, point.y, point.z);
    
        if (inventory.isFull() || item == null){
            doAction("grab at the ground");
        } else {
            doAction("pickup a %s", item.name());
            world.remove(point.x, point.y, point.z);
            inventory.add(item);
            item.setLocation(this);
        }
    }
    
    public void drop(Item item) {
        doAction("drop a " + item.name());
        inventory.remove(item);
        unequip(item);
        world.addAtEmptySpace(item, point.x, point.y, point.z);///???
    }
    
    private void leaveCorpse() {
        Item corpse = new Item('%', color, name + " corpse");
        corpse.modifyFoodValue(maxHp * 3);
        world.addAtEmptySpace(corpse, point.x, point.y, point.z);
    }
    
    public int maxFood() { return maxFood; }

    public int food() { return food; }

    public void eat(Item item){
        doAction("eat a " + item.name());
        if (item.foodValue() < 0)
            notify("Gross!");
         modifyFood(item.foodValue());
         inventory.remove(item);
         unequip(item);
     }


    public void modifyFood(int amount) {
        food += amount;

        if (food > maxFood) {
            maxFood = maxFood + food / 2;
            food = maxFood;
            notify("You can't believe your stomach can hold that much!");
            modifyHp(-1);
        } else if (food < 1 && isPlayer()) {
            modifyHp(-1000);
        }
    }
    
    public Item weapon() { return weapon; }

    public Item armor() { return armor; }

    public void unequip(Item item){
        if (item == null)
           return;
    
        if (item == armor){
            doAction("remove a " + item.name());
            armor = null;
        } else if (item == weapon) {
            doAction("put away a " + item.name());
            weapon = null;
        }
    }

  	public void equip(Item item){
        if (item.attackValue() == 0 && item.defenseValue() == 0)
            return;
    
        if (item.attackValue() >= item.defenseValue()){
            unequip(weapon);
            doAction("wield a " + item.name());
            weapon = item;
        } else {
            unequip(armor);
            doAction("put on a " + item.name());
            armor = item;
        }
    }

    public boolean isPlayer(){
        return glyph == '@';
    }
    
    public void setVision(int vision) {
    	this.visionRadius = vision;
    }
    
	public boolean canSee(int x, int y, int z) {
        return ((Math.abs(getX() - x) <= visionRadius) && (Math.abs(getY() - y) <= visionRadius));
	}

	public int level() { return level; }
	
	public int xp() { return xp; }
	
	public void modifyXp(int amount) {
		xp += amount;
		notify("You %s %d xp.", amount < 0 ? "lose" : "gain", amount);
		while (xp > (int)(Math.pow(level, 1.5) * 20)) {
			level++;
			doAction("ad	vance to level %d", level);
			ai.onGainLevel();
			modifyHp(level * 2);
		}
	}
	public void gainXp(Creature other){
	    int amount = other.maxHp
	      + other.attackValue()
	      + other.defenseValue()
	      - level * 2;

	    if (amount > 0)
	      modifyXp(amount);
	}
	
	public void gainMaxHp() {
	    maxHp += 10;
	    hp += 10;
	    doAction("look healthier");
	  }

	public void gainAttackValue() {
		attackValue += 2;
    	doAction("look stronger");
	}

	public void gainDefenseValue() {
		defenseValue += 2;
    	doAction("look tougher");
	}

	public void gainVision() {
		visionRadius += 1;
    	doAction("look more aware");
	}
}
