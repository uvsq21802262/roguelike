package uvsq.m1info.roguelike.agents;

import java.util.List;

import uvsq.m1info.roguelike.map.Point;
import uvsq.m1info.roguelike.path.Path;
public class ZombieAi extends CreatureAi {
	private Creature player;
	
	public ZombieAi(Creature creature, Creature player) {
		super(creature);
		this.player = player;
	}
	
	public void onUpdate() {
		///*
		if (Math.random() < 0.2)
			return;
		//*/
		if (creature.canSee(player.getX(), player.getY(), player.getZ())) {
			//System.out.println("can see");
			hunt(player);
		}
		else
			wander();
		}
	
	public void hunt(Creature target) {
		List<Point> points = new Path(creature, target.getX(), target.getY())
  				.points();
		int mx = 0;
		int my = 0;
		if( points != null)   
			if (points.isEmpty()) {
				//System.out.println("VIDE");
				return;
			}
			else {
				mx = points.get(0).x - creature.getX();
				my = points.get(0).y - creature.getY();

				System.out.println("\tmove hunter " + this.creature.point + " target " + target.point);
				System.out.println("\tmove : ("+mx+""+my+")");
			}
		//else System.out.println("null");
  
		creature.moveBy(mx, my, 0);
	}

}
