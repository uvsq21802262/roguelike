package uvsq.m1info.roguelike.map;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.items.Item;

public class World {
    private Tile[][][] tiles;
    private int width;
    private int height;
    private int depth;
    private List<Creature> creatures;
    private Item[][][] items;
    //private List<Point> emptyCases;

    public World(Tile[][][] tiles){
        this.tiles = tiles;
        this.width = tiles.length;
        this.height = tiles[0].length;
        this.depth = tiles[0][0].length;
        this.creatures = new ArrayList<Creature>();
        this.items = new Item[width][height][depth];
    }
    
    public int width() { return width; }
    
    public int height() { return height; }
    
    public int depth() { return depth; }
    
    public Tile tile(int x, int y, int z){
        if (x < 0 || x >= width || y < 0 || y >= height || z < 0 || z >= depth)
            return Tile.BOUNDS;
        else
            return tiles[x][y][z];
    }
    
    public List<Creature> creatures(){
    	return creatures;
    }
    
    public Item item(int x, int y, int z) {
        return items[x][y][z];
    }
//ces deux methodes ( plus bas ) n'affichent que l'item si item et monstre sur même case!    
    public char glyph(int x, int y, int z) {
        Creature creature = creature(x, y, z);
        if (creature != null)
            return creature.glyph();
        if (item(x,y,z) != null)
            return item(x,y,z).glyph();
        
        return tile(x, y, z).glyph();
    }

    public Color color(int x, int y, int z) {
        Creature creature = creature(x, y, z);
        if (creature != null)
            return creature.color();
        if (item(x,y,z) != null)
            return item(x,y,z).color();
        
        return tile(x, y, z).color();
    }


    public Creature creature(int x, int y, int z){
        for (Creature c : creatures){
            if (c.getX() == x && c.getY() == y && c.getZ() == z)
                return c;
        }
        return null;
    }

	public void dig(int x, int y, int z) {
	    if (tile(x, y, z).isDiggable())
	        tiles[x][y][z] = Tile.FLOOR;
	}
	
	public void addAtEmptyLocation(Creature creature, int z){
        int x;
        int y;
    
        do {
         x = (int)(Math.random() * width);
         y = (int)(Math.random() * height);
        }
        while (!tile(x,y,z).isGround() || creature(x,y,z) != null);
    
        creature.setPoint(new Point(x, y, z));
        creatures.add(creature);
    }
	
	public void remove(Creature other){
		creatures.remove(other);
	}
	
	public void addAtEmptyLocation(Item item, int depth) {
	    
	    int x;
	    int y;
	    
	    do {
	        x = (int)(Math.random() * width);
	        y = (int)(Math.random() * height);
	    }
	    while (!tile(x,y,depth).isGround() || item(x,y,depth) != null); //peut être sur même case que creature
	    
	    items[x][y][depth] = item;
	    
		//List<Point> points = Point.liste(0, width, 0, height, depth);
	}
	
	public void update(){
	    List<Creature> toUpdate = new ArrayList<Creature>(creatures);
	    for (Creature creature : toUpdate){
	        creature.update();
	    }
	}
	
	public void showCreatures(int z, char c){
		int cpt = 0;
		for (Creature creature : creatures) {
			if (creature.getZ() == z && creature.glyph() == c) {
				System.out.print("("+creature.glyph()+", "+creature.getX()+", "+creature.getY()+", "+creature.getZ()+") | ");
				cpt++;
			}
		}
		System.out.println("");
		System.out.println(cpt);
		
	}

	public void remove(int x, int y, int z) {
		items[x][y][z] = null;
	}
	
	public void addAtEmptySpace(Item item, int x, int y, int z) {
	    if (item == null)
	        return;
	    List<Point> points = new ArrayList<Point>();
	    List<Point> checked = new ArrayList<Point>();
	    points.add(new Point(x, y, z));
	    
	    while (!points.isEmpty()) {
	        Point p = points.remove(0);
	        checked.add(p);
	        if (!tile(p.x, p.y, p.z).isGround())
	            continue;
	        if (items[p.x][p.y][p.z] == null) {
	            items[p.x][p.y][p.z] = item;
	            Creature c = this.creature(p.x, p.y, p.z);
	            if (c != null)
	                c.notify("A %s lands between your feet.", item.name());//forcement le player
	            return;
	        } else {
	            List<Point> neighbors = p.neighbors8();
	            neighbors.removeAll(checked);
	            points.addAll(neighbors);
	        }
	    }

        Creature c = this.creature(x, y, z);
        if (c != null) {
            c.notify("A %s disappears into thin air.", item.name());
            return;
        }
        
        item.setLocation( new Point(x, y, z));
	}

}
