package uvsq.m1info.roguelike.level;
import uvsq.m1info.roguelike.agents.Creature;

import java.util.ArrayList;
import java.util.List;

public class LevelUpController {
	
	private static LevelUpOption[] options = new LevelUpOption[]{
	    new LevelUpOption("Increased hit points"){
	      public void invoke(Creature creature) { creature.gainMaxHp(); }
	    },
	    new LevelUpOption("Increased attack value"){
	      public void invoke(Creature creature) { creature.gainAttackValue(); }
	    },
	    new LevelUpOption("Increased defense value"){
	      public void invoke(Creature creature) { creature.gainDefenseValue(); }
	    },
	    new LevelUpOption("Increased vision"){
	      public void invoke(Creature creature) { creature.gainVision(); }
	    }
	};

	public void autoLevelUp(Creature creature){
		options[(int)(Math.random() * options.length)].invoke(creature);
	}
	
	public List<String> getLevelUpOptions() {
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < options.length; i++) {
			list.add(options[i].name());
		}
		return list;
		
	}

	public LevelUpOption getLevelUpOption(String string) {
		for (int i = 0; i < options.length; i++) 
			if (options[i].name() == string)
				return options[i];
		return null;

	}
	
	
  
}

