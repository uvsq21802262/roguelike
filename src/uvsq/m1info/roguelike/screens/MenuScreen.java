package uvsq.m1info.roguelike.screens;
//bof
import java.awt.event.KeyEvent;

import asciiPanel.AsciiPanel;

public class MenuScreen implements Screen {

	public void displayOutput(AsciiPanel terminal) {
        terminal.write("You won.", 1, 1);
        terminal.writeCenter("press Enter", 23);

        //terminal.write("What would you like to " + getVerb() + "?", 2, 23);
		
	}

	public Screen respondToUserInput(KeyEvent key) {
    	switch (key.getKeyCode()){
			case KeyEvent.VK_ENTER: return null;
    	}
		return this;
	}

}
