
package uvsq.m1info.roguelike.screens;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.map.Tile;
import uvsq.m1info.roguelike.map.World;
import uvsq.m1info.roguelike.map.WorldBuilder;
import uvsq.m1info.roguelike.util.StuffFactory;
import uvsq.m1info.roguelike.agents.Creature;
import asciiPanel.AsciiPanel;

public class PlayScreen implements Screen {
	private final int SCREEN_WIDTH = 80;
    private final int SCREEN_HEIGHT = 21;
	private final int WIDTH = 90;
    private final int HEIGHT = 31;
    private final int DEPTH = 2;
    
    private final int NBFUNGUS = 8;
    private final int NBBATS = 30;
    
    private World world;
    private Creature player;
    private int screenWidth;
    private int screenHeight;
    private List<String> messages;
    private List<List<String>> historic;////ppas implémenté e
    private Screen subscreen;
    
    
    public PlayScreen(){
        screenWidth = SCREEN_WIDTH;
        screenHeight = SCREEN_HEIGHT;
        messages = new ArrayList<String>();
        historic = new ArrayList<List<String>>();
        createWorld();
        StuffFactory factory = new StuffFactory(world);
        createCreatures(factory);
        createItems(factory);
    }
    
    public void displayOutput(AsciiPanel terminal) {

		if (subscreen != null)
		    subscreen.displayOutput(terminal);

        int left = getScrollX();
        int top = getScrollY();
        displayTiles(terminal, left, top);

        String stats = String.format(" %3d/%3d hp (%d,%d,%d) - %8s (%d,%d)", 
        		player.hp(), player.maxHp(), 
        		player.getX(), player.getY(), player.getZ(), 
        		hunger(), player.food(), player.maxFood());
    	terminal.write(stats, 1, 23);
        displayMessages(terminal, messages);
        //terminal.writeCenter("-- press [escape] to lose or [enter] to win --", 22);
    }

    public Screen respondToUserInput(KeyEvent key) {
    	int level = player.level();
    	//add menu screen check inventory
    	if (subscreen != null) {
            subscreen = subscreen.respondToUserInput(key);
        } else {
        	switch (key.getKeyCode()){
				case KeyEvent.VK_ESCAPE: return new LoseScreen();
				case KeyEvent.VK_SPACE: return new WinScreen();
				case KeyEvent.VK_LEFT:
					case KeyEvent.VK_Q: player.moveBy(-1, 0, 0); break;
				case KeyEvent.VK_RIGHT:
					case KeyEvent.VK_D: player.moveBy( 1, 0, 0); break;
				case KeyEvent.VK_UP: player.moveBy( 0, -1, 0); break;
				case KeyEvent.VK_DOWN:
				    case KeyEvent.VK_S: player.moveBy( 0, 1, 0); break;
			    case KeyEvent.VK_A: player.moveBy(-1,-1, 0); break;
			    case KeyEvent.VK_E: player.moveBy( 1,-1, 0); break;
			    case KeyEvent.VK_W: player.moveBy(-1, 1, 0); break;
			    case KeyEvent.VK_C: player.moveBy( 1, 1, 0); break;
			    case KeyEvent.VK_R:
					if (userIsTryingToExit())
			        	return userExits();
			        else
			        	player.moveBy( 0, 0, -1); 
			        break;
			    case KeyEvent.VK_F: player.moveBy( 0, 0, 1); break;
			    case KeyEvent.VK_T: player.pickup(); break;

		        //case KeyEvent.VK_M: subscreen = new MenuScreen(); break;
		        case KeyEvent.VK_V: subscreen = new EatScreen(player); break;
		        case KeyEvent.VK_B: subscreen = new EquipScreen(player); break;
		        case KeyEvent.VK_N: subscreen = new DropScreen(player); break;
	        }
    	}
        //world.showCreatures(player.z, 'b');
	    if (subscreen == null) 
	    	world.update();
	    if (player.level() > level)
	        subscreen = new LevelUpScreen(player, player.level() - level);
        if (player.hp() < 1) {
        	return new LoseScreen();
        }
        return this;
    }
    
    public int getScrollX() {
        return Math.max(0, Math.min(player.getX() - screenWidth / 2, world.width() - screenWidth));
    }

    public int getScrollY() {
        return Math.max(0, Math.min(player.getY() - screenHeight / 2, world.height() - screenHeight));
    }

    private void displayTiles(AsciiPanel terminal, int left, int top) {
        for (int x = 0; x < screenWidth; x++){
            for (int y = 0; y < screenHeight; y++){
                int wx = x + left;
                int wy = y + top;
                terminal.write(world.glyph(wx, wy, player.getZ()), x, y, world.color(wx, wy, player.getZ()));
            }
        }
        for (Creature creature : world.creatures()) {
        	if (creature.getX() >= left && creature.getX() < (left + screenWidth) && creature.getY() >= top && creature.getY() < (top + screenHeight) && creature.getZ() == player.getZ()) {
                terminal.write(creature.glyph(), creature.getX() - left, creature.getY() - top, creature.color());
        	}
        }
    }
    
    private void displayMessages(AsciiPanel terminal, List<String> messages) {
        int top = screenHeight - messages.size();
        for (int i = 0; i < messages.size(); i++){
            terminal.writeCenter(messages.get(i), top + i);
        }
        historic.add(messages);
        messages.clear();
    }
    
    private void createWorld(){
        world = new WorldBuilder(WIDTH, HEIGHT,DEPTH)
              .makeCaves()
              .build();
    }
    
    private void createCreatures(StuffFactory factory){
        player = factory.newPlayer(messages, 0);// niveau 0
        for (int z = 0; z < world.depth(); z++) {
	        for (int i = 0; i < NBFUNGUS; i++){
	            factory.newFungus(z);
	        }
	        for (int i = 0; i < NBBATS; i++){
	            factory.newBat(z);
	        }
	        for (int i = 0; i < z + 3; i++){
	            factory.newZombie(z, player);
	        }
        }
    }

	private void createItems(StuffFactory factory) {

	    factory.newVictoryItem(world.depth() - 1);
	    for (int z = 0; z < world.depth(); z++){
	        for (int i = 0; i < world.width() * world.height() / 20; i++){
	            factory.newRock(z);//tester lorsque plus de place
	        }
	    }
	    for (int z = 0; z < world.depth(); z++){
	    	factory.randomArmor(z);
	    	factory.randomWeapon(z);
	    }
	    factory.newEdibleWeapon(0);
	}
	
	private boolean userIsTryingToExit() {
	    return player.getZ() == 0 && world.tile(player.getX(), player.getY(), player.getZ()) == Tile.STAIRS_UP;
	}
	
	private Screen userExits() {
	    for (Item item : player.inventory().getItems()){
	        if (item != null && item.name().equals("teddy bear"))
	            return new WinScreen();
	    }
	    return new LoseScreen();
	}
	
	private String hunger(){
	    if (player.food() < player.maxFood() * 0.1)
	        return "Starving";
	    else if (player.food() < player.maxFood() * 0.2)
	        return "Hungry";
	    else if (player.food() > player.maxFood() * 0.9)
	        return "Stuffed";
	    else if (player.food() > player.maxFood() * 0.8)
	        return "Full";
	    else
	        return "Ok";
	}

}